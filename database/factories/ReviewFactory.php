<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Review;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'content' => $faker->text,
        'status' => $faker->numberBetween($min=0,$max=1),
        'author_id' => User::inRandomOrder()->take(1)->get()->first()->id,
    ];
});
