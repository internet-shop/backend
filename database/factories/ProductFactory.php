<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use Faker\Provider\Base;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->words(random_int(1,3),true),
        'price' => $faker->numberBetween($min = 500, $max = 9000),
        'description' => $faker->text,
        'popularity' => $faker->numberBetween($min = 0, $max = 500),
        'amount' => $faker->numberBetween($min = 0, $max = 500),
        //'type' => $faker->numberBetween($min = 0, $max = 3),
    ];
});
