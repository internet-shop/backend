<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Гортензии',
            'type' => 'flower',
        ],[
            'name' => 'Тюльпаны',
            'type' => 'flower',
        ],[
            'name' => 'Орхидеии',
            'type' => 'flower',
        ],[
            'name' => 'Хризантемы',
            'type' => 'flower',
        ],[
            'name' => 'Розы',
            'type' => 'flower',
        ],[
            'name' => 'Герберы',
            'type' => 'flower',
        ],[
            'name' => 'Ирисы',
            'type' => 'flower',
        ],[
            'name' => 'Гвоздики',
            'type' => 'flower',
        ],[
            'name' => 'Лилии',
            'type' => 'flower',
        ],[
            'name' => 'Корзинка',
            'type' => 'add',
        ],[
            'name' => 'Коробка',
            'type' => 'add',
        ],[
            'name' => 'Пробирка',
            'type' => 'add',
        ],[
            'name' => 'Бумага',
            'type' => 'add',
        ],[
            'name' => 'Лента',
            'type' => 'add',
        ],[
            'name' => 'Красная',
            'type' => 'color',
        ],[
            'name' => 'Белая',
            'type' => 'color',
        ],[
            'name' => 'Фиолетовая',
            'type' => 'color',
        ],[
            'name' => 'Оранжевая',
            'type' => 'color',
        ],[
            'name' => 'Розовая',
            'type' => 'color',
        ],[
            'name' => 'Смешанная',
            'type' => 'color',
        ]);
    }
}
