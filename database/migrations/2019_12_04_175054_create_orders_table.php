<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
//            $table->bigIncrements('id');
//            $table->bigInteger('user_id');
//            $table->string('first_name');
//            $table->string('last_name');
//            $table->string('phone_number');
//            $table->string('email');
//            $table->bigInteger('product_id');
//            $table->bigInteger('product_amount')->default(1);
//            $table->string('type')->nullable();
//            $table->string('delivery_address')->nullable();
//            $table->string('paid')->default(0);
//            $table->string('status');
//            $table->timestamps();

            $table->bigIncrements('id');
            $table->bigInteger('order_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');

            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_number')->nullable();
            $table->string('email');

            $table->unsignedBigInteger('product_id');

            $table->foreign('product_id')
                ->references('id')->on('products')->onDelete('cascade');

            $table->string('amount');
            $table->string('paid')->default(0);
            $table->string('type')->nullable(); // delivery,pickup
            $table->string('delivery_address')->nullable();
            $table->date('date')->nullable();
            $table->string('status'); // 1: in progress, 2: done(history)
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
