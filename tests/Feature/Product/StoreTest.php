<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Optix\Media\MediaUploader;
use Optix\Media\Models\Media;
use Tests\TestCase;

class StoreTest extends TestCase
{
    public function testMediaService()
    {
        $media = [
            MediaUploader::fromFile(UploadedFile::fake()->image('file-name-1.png'))->upload()->id,
            MediaUploader::fromFile(UploadedFile::fake()->image('file-name-2.png'))->upload()->id,
            MediaUploader::fromFile(UploadedFile::fake()->image('file-name-3.png'))->upload()->id,
        ];

        $data = [
            'name' => $this->faker->words(random_int(1,3),true),
            'price' => $this->faker->numberBetween($min = 500, $max = 9000),
            'description' => $this->faker->text,
            'popularity' => $this->faker->numberBetween($min = 0, $max = 500),
            'amount' => $this->faker->numberBetween($min = 0, $max = 500),
            'media' => $media,
        ];

        $response = $this
            ->actingAs($this->user, 'api')
            ->post(
                route('products.store'),
                $data,
                [
                    'Accept' => 'application/json'
                ]
            );

        $response
            //->dump()
            ->assertStatus(201)
            ->assertJsonStructure([
                'message', 'product'
            ]);
    }
}
