<?php

namespace Tests\Feature\Media;

use Illuminate\Http\UploadedFile;
use Optix\Media\Models\Media;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreTest extends TestCase
{
    public function testSuccess()
    {
        $data = [
            'file' => UploadedFile::fake()->image('file-name-1.jpg'),
        ];

        $response = $this->actingAs($this->user, 'api')->post(
            route('files.store'),
            $data,
            [
                'Accept' => 'application/json'
            ]
        );

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                'message', 'media'
            ]);

        $media = Media::find($response->getData()->media->id);

        $this->assertTrue($media->filesystem()->exists($media->getPath()));
    }
}
