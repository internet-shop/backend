<?php

namespace Tests;

use Faker\Factory as Faker;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseMigrations;
    use DatabaseTransactions;

    protected $faker;
    protected $user;
    protected $anotherUser;
    protected $admin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate');
        //$this->artisan('db:seed');

        $this->user = factory(User::class)->create([
            'role' => 'user'
        ]);

        $this->anotherUser = factory(User::class)->create([
            'role' => 'user'
        ]);

        $this->admin = factory(User::class)->create([
            'role' => 'admin'
        ]);

        $this->faker = Faker::create();
    }
}
