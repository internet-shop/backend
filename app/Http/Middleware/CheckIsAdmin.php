<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->check()) {
            return response()->json([
                'message' => 'Вы не авторизированны.',
            ], 401);
        }

        $authUser = auth()->user();

        if($authUser->role !== 'admin'){
            return response()->json([
                'message' => 'Недостаточно прав.',
            ], 403);
        }

        return $next($request);
    }
}
