<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\Order;
use App\Models\User as User;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Http\Middleware\CheckIsAdmin;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('user.is_admin')->only(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return UserResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $user = User::create(
            $request->validated()
        );

        return response()->json([
            'message' => 'Пользователь был успешно создан.',
            'user' => new UserResource($user),
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param User $user
     * @return Response
     * @throws AuthorizationException
     */
    public function update(UpdateRequest $request, User $user)
    {
        $this->authorize('update', $user);

        $user->update(
            $request->validated()
        );

        return response()->json([
            'message' => 'Данные пользователя обновлены.',
            'user' => new UserResource($user)
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            'message' => 'Пользователь был успешно удален.',
            'user' => new UserResource($user),
        ],201);
    }

    public function favourites()
    {
        $authUser = auth()->user();
        $favourites = User::find($authUser->id)->favourites()->get();
        return response()->json([
            'response' => $favourites
        ]);
    }

    public function history()
    {
        $authUser = auth()->user();
        $history = Order::all()->where('user_id', $authUser->id)->where('status', 2)->groupBy('order_id');

        return response()->json([
            $history
        ]);
    }

    public function authuser()
    {
        return response()->json([
            'user' => new UserResource(auth()->user())
        ]);
    }
}
