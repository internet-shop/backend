<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\GuestPurchaseRequest;
use App\Http\Requests\Orders\PurchaseRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Faker\Generator as Faker;
use phpDocumentor\Reflection\Types\Integer;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['guestPurchase']);
        $this->middleware('user.is_admin')->only(['complete', 'delete', 'index']);
    }

    public function index()
    {
        return Order::all();
    }

    public function purchase(PurchaseRequest $request, Faker $faker)
    {
        $authUser = auth()->user();
        $positions = Order::all()->where('user_id', $authUser->id)->where('status', 0);
        $validated = $request->validated();
        $order_id = $faker->numberBetween($min = 10000, $max = 99999);

        if($positions == '[]'){
            return response()->json([
                'message' => 'Корзина пуста!',
            ]);
        }

        foreach ($positions as $position){
            $position->update([
                'order_id' => $order_id,
                'paid' => $validated['paid'],
                'type' => $validated['type'],
                'phone_number' => $validated['phone_number'],
                'delivery_address' => $validated['delivery_address'],
                'date' => date('Y-m-d'),
                'status' => 1,
            ]);

            $product = Product::find($position->product_id);
            $product->popularity++;
            $product->save();
        }

        $authUser->update(['orders' => $authUser->orders + 1, 'phone_number' => $validated['phone_number']]);

        $order = Order::all()->where('order_id', $order_id);

        return response()->json([
            'message' => $order->load('product')
        ]);
    }

    public function guestPurchase(GuestPurchaseRequest $request, Faker $faker)
    {
        $validated = $request->validated();
        $order_id = $faker->numberBetween($min = 10000, $max = 99999);
        $cart = $validated['cart'];

        foreach ($cart as $item => $amount){
            Order::create([
                'first_name' => $validated['first_name'],
                'last_name' => $validated['last_name'],
                'email' => $validated['email'],
                'order_id' => $order_id,
                'product_id' => $item,
                'amount' => $amount,
                'paid' => $validated['paid'],
                'type' => $validated['type'],
                'delivery_address' => $validated['delivery_address'],
                'date' => date('Y-m-d'),
                'status' => 1,
            ]);
        }

        $order = Order::all()->where('order_id', $order_id);
        return response()->json([
            'message' => $order->load('product')
        ]);
    }

    public function fromCart($product_id)
    {
        $position = Order::all()->where('user_id', auth()->user())->where('product_id', $product_id);
        $position->delete();
    }

    public function complete($order_id)
    {
        $positions = Order::all()->where('order_id', $order_id)->where('status', 1);

        if($positions == '[]'){
            return response()->json([
                'message' => 'Заказ не найден.',
            ]);
        }

        foreach ($positions as $position){
            $position->update([
                'status' => 2,
            ]);
        }

        return response()->json([
            'message' => 'Заказ отмечен выполненым.'
        ]);
    }

    public function delete($order_id)
    {
        $positions = Order::all()->where('order_id', $order_id)->where('status', 2);

        foreach ($positions as $position){
            $position->delete();
        }

        return response()->json([
            'message' => 'Заказ удален.'
        ]);
    }
}
