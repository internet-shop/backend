<?php

namespace App\Http\Controllers;

use Exception;
use Optix\Media\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Optix\Media\MediaUploader;
use App\Http\Requests\File\StoreRequest;
use App\Http\Requests\File\UpdateRequest;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('user.is_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            Media::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();

        $file = MediaUploader::fromFile($validated['file'])->upload();

        return response()->json([
            'message' => 'Файл был успешно загружен.',
            'media' => $file,
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Media $file
     * @return Response
     */
    public function show(Media $file)
    {
        return response()->json([
            'media' => $file,
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Media $file
     * @return Response
     */
    public function update(UpdateRequest $request, Media $file)
    {
        $validated = $request->validated();

        $file->update(
            $validated
        );

        return response()->json([
            'message' => 'Файл был успешно обновлён.',
            'media' => $file,
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Media $file
     * @return void
     * @throws Exception
     */
    public function destroy(Media $file)
    {
        $file->delete();

        return response()->json([
            'message' => 'Файл был успешно удалён.',
            'media' => $file,
        ]);
    }
}
