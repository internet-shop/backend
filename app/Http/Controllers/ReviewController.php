<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReviewResource;
use App\Models\Review;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Http\Requests\Review\StoreRequest;
use App\Http\Requests\Review\UpdateRequest;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:update,review')->only(['update']);
        $this->middleware('can:delete,review')->only(['destroy']);
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection|Response
     */
    public function index()
    {
        return ReviewResource::collection(Review::all());
    }

    public function accepted()
    {
        return ReviewResource::collection(Review::all()->where('status', 1));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $review = Review::create(
            $request->validated()
        );

        return response()->json([
            'message' => 'Вы успешно оставили отзыв.',
            'user' => new ReviewResource($review)
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Review $review
     * @return ReviewResource
     */
    public function show(Review $review)
    {
        return new ReviewResource($review);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Review $review
     * @return Response
     */
    public function update(UpdateRequest $request, Review $review)
    {
        $review->update(
            $request->validated()
        );

        return response()->json([
            'message' => 'Вы успешно изменили отзыв.',
            'post' => new ReviewResource($review)
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Review $review
     * @return Response
     * @throws Exception
     */
    public function destroy(Review $review)
    {
        $review->delete();

        return response()->json([
            'message' => 'Вы успешно удалили отзыв.',
            'user' => new ReviewResource($review),
        ],201);
    }
}
