<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use App\Http\Requests\Product\StoreRequest;
use App\Http\Requests\Product\UpdateRequest;
use Illuminate\Support\Str;
use App\Services\MediaService;
use Optix\Media\Models\Media;

class ProductController extends Controller
{
    private $mediaService;

    public function __construct(MediaService $mediaService)
    {
        $this->middleware('auth:api')->except(['popular','index', 'filter', 'categories', 'show']);
        $this->middleware('user.is_admin')->only(['store', 'update', 'destroy']);
        $this->mediaService = $mediaService;
    }
        /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        $validated = $request->validated();

        $slug = $request->input('name');
        $slug = Str::slug($slug);
        if(Product::where('slug', $slug)->exists()){
            return response()->json([
                'message' => 'Ошибка! Такое название уже используется.'
            ]);
        }

        $product = Product::create(
            $request->validated()
        );

        if(isset($validated['media'])){
            $this->mediaService->attachFiles($validated['media'], $product, 'product');
        }

        return response()->json([
            'message' => 'Продукт был успешно создан.',
            'product' => new ProductResource($product),
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return (new ProductResource($product));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param Product $product
     * @return void
     */
    public function update(UpdateRequest $request, Product $product)
    {
        $product->update(
            $request->validated()
        );

        return response()->json([
            'message' => 'Продукт был успешно изменён.',
            'product' => new ProductResource($product)
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return Response
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json([
            'message' => 'Продукт был успешно удалён.',
            'product' => new ProductResource($product),
        ],201);
    }

    public function popular()
    {
        return response()->json([
            'response' => ProductResource::collection(Product::orderBy('popularity', 'desc')->take(3)->get())
        ]);
    }

    public function categories(Product $product)
    {
        return response()->json([
            'response' => $product->categories()->get()
        ]);
    }

    public function toFavourites(Product $product)
    {
        $product->users()->attach(auth()->user());
    }

    public function unFavourite(Product $product)
    {
        $product->users()->detach(auth()->user());
    }

    public function toCart(Product $product, Request $request)
    {
        $amount = $request->input('amount');
        $authUser = auth()->user();
        $position = Order::all()->where('user_id', auth()->user()->id)->where('product_id', $product->id)->where('status', 0);

        if($position->count() > 0){
            (new Order($position->toArray()))->update(['amount' => $position->pluck('amount')->first() + $amount]);

            return response()->json([
                'message' => 'Товар успешно добавлен в корзину.'
            ]);
        }

        Order::create([
            'user_id' => $authUser->id,
            'first_name' => $authUser->first_name,
            'last_name' => $authUser->last_name,
            'phone_number' => $authUser->phone_number,
            'email' => $authUser->email,
            'product_id' => $product->id,
            'amount' => $amount,
            'status' => 0,
        ]);

        return response()->json([
            'message' => 'Товар успешно добавлен в корзину.'
        ]);
    }

    public function fromCart($product_id)
    {
        $position = Order::all()->where('user_id', auth()->user()->id)->where('product_id', $product_id)->toArray();
        (new Order($position))->delete();
    }

    public function filter(Request $request)
    {
        $flowers = $request->flowers;
        $colors = $request->colors;
        $adds = $request->adds;

        $products = Product::with('categories');

        if($request->has('flowers')){
            foreach ($flowers as $flower){
                $products->whereHas('categories', function ($query) use ($flower){
                        $query->where('name', 'like', $flower);
                });
            }
        }

        if($request->has('min')){
            $products->where('price', '>' , $request->min - 1);
        }

        if($request->has('max')){
            $products->where('price', '<', $request->max + 1);
        }

        if($request->has('adds')){
            foreach ($adds as $add) {
                $products->whereHas('categories', function ($query) use ($add) {
                    $query->where('name', 'like', $add);
                });
            }
        }

        if($request->has('colors')){
            foreach ($colors as $color) {
                $products->whereHas('categories', function ($query) use ($color) {
                    $query->where('name', 'like', $color);
                });
            }
        }

        return response()->json($products->get());
    }
}
