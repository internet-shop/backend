<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'status' => $this->status,
            'created_at' => Carbon::parse($this->created_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
            'updated_at' => Carbon::parse($this->updated_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
            'author_id' => $this->author_id,
        ];
    }
}
