<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => Str::Title($this->first_name),
            'last_name' => Str::Title($this->last_name),
            'email' => $this->email,
            'role' => $this->role,
            'orders' => $this->orders,
            'phone_number' => $this->when($this->phone_number,'+' . $this->phone_number,null),
            'created_at' => Carbon::parse($this->created_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
            'updated_at' => Carbon::parse($this->updated_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
        ];
    }
}
