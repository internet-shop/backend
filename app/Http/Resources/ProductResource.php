<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Optix\Media\Models\Media;

class ProductResource extends JsonResource
{
    protected $urls = [];

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $media = $this->media->pluck('id');

        foreach ($media as $m){
            $url = Media::find($m)->getUrl('thumb');
            array_push($this->urls, $url);
        }

        return [
            'id' => $this->id,
            'name' => Str::title($this->name),
            'description' => $this->description,
            'price' => $this->price,
            'popularity' => $this->popularity,
            'amount' => $this->amount,
            'slug' => $this->slug,
            'created_at' => Carbon::parse($this->created_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
            'updated_at' => Carbon::parse($this->updated_at)->isoFormat('Do MMMM YYYY года, в H:mm:ss'),
            'media' => $this->media,
            'media_urls' => $this->urls,
            'liked' => $this->when(in_array(auth()->user()->id ?? 0, $this->users->pluck('id')->toArray() ?? [0]), 'true', 'false'),//$this->users->pluck('id'),
            'categories' => $this->categories,
        ];
    }
}
