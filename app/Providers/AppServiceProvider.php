<?php

namespace App\Providers;

use App\Observers\ProductObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Models\Product;
use Intervention\Image\Image;
use Optix\Media\Facades\Conversion;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Product::observe(ProductObserver::class);

        if (config("app.is_secure")) {
            \URL::forceScheme('https');
        }
        Schema::defaultStringLength(191);

        Conversion::register('thumb', function (Image $image) {
            return $image->fit(385, 385);
        });
    }
}
