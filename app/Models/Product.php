<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Optix\Media\HasMedia;

class Product extends Model
{
    use HasMedia;

    protected $fillable = [
        'name', 'price', 'description', 'popularity', 'amount', 'slug'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'product_user');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'product_id');
    }

    public function registerMediaGroups()
    {
        $this->addMediaGroup('product')->performConversions('thumb');
    }

    public function popularProducts() {
        return $this->orderBy('popularity', 'desc')->take(3);
    }
}
