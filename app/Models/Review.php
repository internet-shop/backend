<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Review extends Model
{
    protected $fillable = [
        'id', 'content', 'author_id', 'status', 'created_at', 'updated_at'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }
}
