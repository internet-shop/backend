<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'name', 'status'
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product');
    }
}
