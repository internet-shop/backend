<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Review;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role', 'phone_number', 'orders', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token', 'updated_at', 'created_at'
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function isAdmin()
    {
        return $this->where('role', 'admin')->exists();
    }

    public function reviews()
    {
        return $this->hasMany(Review::class, 'author_id');
    }

    public function favourites()
    {
        return $this->belongsToMany(Product::class, 'product_user');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }
}
