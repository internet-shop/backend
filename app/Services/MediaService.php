<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Optix\Media\MediaUploader;
use Optix\Media\Models\Media;
use Optix\Media\HasMedia;

class MediaService
{
    public $media;
    public $files;
    public $group;

    /**
     * @param UploadedFile $file
     * @return Media
     */
    public function uploadFile(UploadedFile $file): Media
    {
        $media = MediaUploader::fromFile($file)->upload();

        return $media;
    }

    /**
     * @param array $files
     * @return Collection
     */
    public function uploadFiles(array $files): Collection
    {
        $this->files = $files;
        $media = [];

        foreach ($files as $file) {
            $media[] = $this->uploadFile($file);
        }

        // возвращаем коллекцию Media, чтобы можно было в дальнейшем работать с данными как с коллекцией
        $this->media = (new Collection())->merge($media);

        return $this->media;
    }

    public function attachFiles(array $files, Model $model, $group = 'default')
    {
        $model->detachMedia();
        $media = $model->attachMedia($files, $group);

        $this->media = (new Collection())->merge($media);

        return $this->media;
    }

    public function getUrls(array $media)
    {

    }
}
