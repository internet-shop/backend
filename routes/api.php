<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
    Route::get('popular', 'ProductController@popular');
    Route::post('filter', 'ProductController@filter');
    Route::get('{product}/categories', 'ProductController@categories');
    Route::post('{product}/tocart', 'ProductController@toCart');
    Route::get('{product}/tofavourites', 'ProductController@toFavourites');
    Route::get('{product}/unfavourite', 'ProductController@unFavourite');
    Route::delete('{product_id}/fromcart', 'ProductController@fromCart');
});

Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
    Route::get('/', 'OrderController@index');
    Route::put('purchase', 'OrderController@purchase');
    Route::put('{order_id}/complete', 'OrderController@complete');
    Route::delete('{order_id}/delete', 'OrderController@delete');
    Route::delete('{product_id}/fromCart', 'OrderController@fromCart');
    Route::post('guestpurchase', 'OrderController@guestPurchase');
});

Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
    Route::get('favourites', 'UserController@favourites');
    Route::get('history', 'UserController@history');
    Route::get('authuser', 'UserController@authuser');
});

Route::group(['prefix' => 'reviews', 'as' => 'reviews.'], function () {
    Route::get('accepted', 'ReviewController@accepted');
});

Route::apiResources([
    'users' => 'UserController',
    'products' => 'ProductController',
    'reviews' => 'ReviewController',
    'categories' => 'CategoryController',
    'files' => 'FileController',
]);

Route::group(['prefix' => 'auth', 'as' => 'auth.'], function (){
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('register', 'Auth\RegisterController@register')->name('register');
    Route::middleware('auth:api')->get('logout', 'Auth\LoginController@logout')->name('logout');
});
